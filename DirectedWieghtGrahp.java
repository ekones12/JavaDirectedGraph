import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

public class DirectedWieghtGrahp<String>{
	
	private final Map<Vertex<String>,Set<Vertex<String>>> graph;
	private ArrayList<String> control;
	public int railnum=0;
	
	 public DirectedWieghtGrahp() {
	        graph = new HashMap<Vertex<String> ,Set<Vertex<String>>>();
	    	control = new ArrayList<String>();
	   }
	public class Vertex<String> {
	    public String item;
	    boolean underMaintenance;
	    public int totalpass;
	    private ArrayList<Edge<String>> edges;
	}
	public class Edge<String> {
	    public Vertex<String> source;
	    public Vertex<String> destination;
	    boolean isBroken ;
	    boolean isActive;
	    private double weight;
	}
	
	public void addVertex(String vertexname) {
		if(!control.contains(vertexname)) {
			control.add(vertexname);
			Vertex<String> nev = new Vertex<String>();
			nev.item= vertexname;
			nev.underMaintenance=false;
			nev.edges = new ArrayList<Edge<String>>();
			graph.put(nev, new HashSet<Vertex<String>>());
		}
	}

	public void addEdge(Vertex<String> source,Vertex<String> destination,double weight) {	
		Edge<String> nevSource = new Edge<String>();
		nevSource.source = source;
		nevSource.destination = destination;
		nevSource.isBroken = false;
		nevSource.isActive=false;
		nevSource.weight=weight;
		
		Edge<String> nevDestination = new Edge<String>();
		nevDestination.source = destination;
		nevDestination.destination = source;
		nevDestination.isBroken = false;
		nevDestination.isActive=false;
		nevDestination.weight=weight;
		
		source.edges.add(nevSource);
		destination.edges.add(nevDestination);
		
		graph.get(source).add(destination);
		graph.get(destination).add(source);
		railnum++;railnum++;
		
	}
	
	public Vertex<String> getVertex(String name){
		for(Vertex<String > grap : graph.keySet()) {
			if (grap.item.equals(name)) {
				//System.out.println("burda bulunasSdu found\n"+grap.item);
				return grap;
			}	
		}	
		return null;
	}

	public Edge<String> getVertexEdge(String name,String dest){
		for(Vertex<String > grap : graph.keySet()) {
			if (grap.item.equals(name)) {
				for (int i=0;i<grap.edges.size();i++) {
		        	if(grap.edges.get(i).destination.item.equals(dest)) {
		            		 //System.out.println("�nce "+grap.edges.get(i).isActive);
		            	return grap.edges.get(i);
		            		 //System.out.println("sonra "+grap.edges.get(i).isActive);
		            	}
		            }
				}
			}	
		return null;
	}
	public Edge<String> ChangeActiveEdge(String name,String dest){
		for(Vertex<String > grap : graph.keySet()) {
			if (grap.item.equals(name)) {
				for (int i=0;i<grap.edges.size();i++) {
					grap.edges.get(i).isActive=false;
		        	if(grap.edges.get(i).destination.item.equals(dest)) {
		            	grap.edges.get(i).isActive=true;
		            		 //System.out.println("sonra "+grap.edges.get(i).isActive);
		            	}
		            }
				}
			}	
		return null;
	}
	
	public void listRoutes(String name){
		for(Vertex<String > grap : graph.keySet()) {
			if (grap.item.equals(name)) {
				System.out.print("\tRoutes from "+name+":");
				
				TreeSet <String> sorted = new TreeSet<String>();
				for (int i=0;i<grap.edges.size();i++) {
					sorted.add(grap.edges.get(i).destination.item);	
		            }
				Iterator iterator;
		        iterator = sorted.iterator();
		        //Displaying the Tree set data
		        while (iterator.hasNext()){
		            System.out.print(iterator.next() + " ");
		        }
		        
				System.out.print("\n");
				}
			}	

	}
	
	public void undermaintain(){
		
		TreeSet <String> sorted = new TreeSet<String>();
		for(Vertex<String > grap : graph.keySet()){
			if (grap.underMaintenance) {
				sorted.add(grap.item);
				}
			}
		Iterator iterator;
        iterator = sorted.iterator();
        //Displaying the Tree set data
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }System.out.print("\n");	

	}
	public void activerail(){
		
		TreeSet <String> sorted = new TreeSet<String>();
		for(Vertex<String > grap : graph.keySet()) {
				for (int i=0;i<grap.edges.size();i++) {
		        	if(grap.edges.get(i).isActive) {
		        		StringBuilder adder = new StringBuilder();
		        		adder.append(grap.edges.get(i).source.item);
		        		adder.append(">");
		        		adder.append(grap.edges.get(i).destination.item);
		            	sorted.add((String) adder.toString());	 
		            	}
		            }
			}	
		Iterator iterator;
        iterator = sorted.iterator();
        //Displaying the Tree set data
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }System.out.print("\n");	
	}
		public void brokenrail(){
			
			TreeSet <String> sorted = new TreeSet<String>();
			for(Vertex<String > grap : graph.keySet()) {
					for (int i=0;i<grap.edges.size();i++) {
			        	if(grap.edges.get(i).isBroken==true) {
			        		StringBuilder adder = new StringBuilder();
			        		adder.append(grap.edges.get(i).source.item);
			        		adder.append(">");
			        		adder.append(grap.edges.get(i).destination.item);
			            	sorted.add((String) adder.toString());	 
			            	}
			            }
				}	
			Iterator iterator;
		    iterator = sorted.iterator();
		    //Displaying the Tree set data
		    while (iterator.hasNext()){
		        System.out.print(iterator.next() + " ");
		    }System.out.print("\n");	
		}
		public void crosstime(){
			
			TreeSet <String> sorted = new TreeSet<String>();
			for(Vertex<String > grap : graph.keySet()){
				if (grap.totalpass>0) {
					StringBuilder adder = new StringBuilder();
	        		adder.append(grap.item);
	        		adder.append(":");
	        		adder.append(grap.totalpass);
	            	sorted.add((String) adder.toString());	
					}
				}
			Iterator iterator;
	        iterator = sorted.iterator();
	        //Displaying the Tree set data
	        while (iterator.hasNext()){
	            System.out.print(iterator.next() + " ");
	        }System.out.print("\n");	

		}

	public int size() {
		return control.size();
	}
	
	public void dijkstra() {
		
	}
}

