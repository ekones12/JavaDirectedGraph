import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class Assignment4 {
	
	public static ArrayList<String> reader (String args1) {
		
	    BufferedReader br = null;
	    String line = "";
	   	ArrayList<String> dizi = new ArrayList<String>();
	  
	    try {
	        br = new BufferedReader(new FileReader(args1));
	        while ((line = br.readLine()) != null) {
	        		if(!("".equals(line))) {
	        			dizi.add(line);
	        		}
	        }
	
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    return dizi;
	}
	public static void railway(ArrayList<String> dizi,DirectedWieghtGrahp<String> graph ) {
		
		for (int i = 0; i<dizi.size();i++) {
			String item = dizi.get(i);
			//System.out.println(item);
			String itemlist[]= item.split("-");
			//System.out.println(itemlist[0]+"-------"+itemlist[1]);
			String dest[] = itemlist[1].split(" ");
		//	System.out.println(dest[0]+"!!!!!!"+dest[1]);
			graph.addVertex(itemlist[0]);
			graph.addVertex(dest[0]);
			graph.addEdge(graph.getVertex(itemlist[0]),graph.getVertex(dest[0]), Double.parseDouble(dest[1]));
		}
		
	}
	public static  void topology(ArrayList<String> topology,DirectedWieghtGrahp<String> graph ) {
		
		for (int i = 0; i<topology.size();i++) {
			String item = topology.get(i);
			//System.out.println(item);
			String itemlist[]= item.split(":");
			String dest[] = itemlist[1].split(">");
			graph.getVertexEdge(itemlist[0], dest[1]).isActive=true;
			//System.out.println(graph.getVertexEdge(itemlist[0], dest[1]).isActive + graph.getVertexEdge(itemlist[0], dest[1]).destination.item);
		}
		
	}
	
	
	public static void commands(ArrayList<String> command,DirectedWieghtGrahp<String> graph ) {
		for(int i = 0 ; i<command.size();i++) {
			String item = command.get(i);
			System.out.println("COMMAND IN PROCESS >> "+item);
			String itemlist[]=item.split(" ");
			if(itemlist[0].equals("ROUTE")) {
				
			}
			else if(itemlist[0].equals("REPAIR")) {
				String itemlist2[] = itemlist[1].split(">");
				graph.getVertexEdge(itemlist2[0],itemlist2[1]).isBroken=false;
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
			}
			else if(itemlist[0].equals("BREAK")) {
				String itemlist2[] = itemlist[1].split(">");
				graph.getVertexEdge(itemlist2[0],itemlist2[1]).isBroken=true;
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
			}
			else if(itemlist[0].equals("MAINTAIN")) {
				graph.getVertex(itemlist[1]).underMaintenance=true;
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
			}
			else if(itemlist[0].equals("SERVICE")) {
				graph.getVertex(itemlist[1]).underMaintenance=false;
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
			}
			else if(itemlist[0].equals("LISTROUTESFROM")) {
				graph.listRoutes(itemlist[1]);
				
				
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
				
			}	
			else if(itemlist[0].equals("ADD")) {
				graph.addVertex(itemlist[1]);
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
				
			}
			else if(itemlist[0].equals("LISTMAINTAINS")) {
				System.out.print("\tIntersections under maintenance: ");
				graph.undermaintain();
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
				
			}
			else if(itemlist[0].equals("LINK")) {
				String itemlistlink[]= itemlist[1].split(":");
				String destlink[] = itemlistlink[1].split(">");
				String destinationlink[]=destlink[0].split(",");
				for(int j =0;j<destinationlink.length;j++) {
					System.out.println(destinationlink.length);
					String destinations[]=destinationlink[j].split("-");
					graph.addEdge(graph.getVertex(itemlistlink[0]),graph.getVertex(destinations[0]), Double.parseDouble(destinations[1]));
				}
				graph.ChangeActiveEdge(itemlistlink[0], destlink[1]);
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
			}
			else if(itemlist[0].equals("LISTACTIVERAILS")) {
				
				
				System.out.print("\tActive Rails: ");
				graph.activerail();
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
				
			}
			else if(itemlist[0].equals("LISTBROKENRAILS")) {
				System.out.print("\tBroken Rails: ");
				graph.brokenrail();
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
			}
			else if(itemlist[0].equals("LISTCROSSTIMES")) {
				System.out.print("\t# of cross times: ");
				graph.crosstime();
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
				
			}
			else if(itemlist[0].equals("TOTALNUMBEROFJUNCTIONS")) {
				System.out.print("\tTotal # of junctions: ");
				System.out.println(graph.size());
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
			}
			else if(itemlist[0].equals("TOTALNUMBEROFRAILS")) {
				System.out.print("\tTotal # of rails: ");
				System.out.println(graph.railnum);
				System.out.println("\tCommand \""+item +"\" has been executed successfully!");
				
			}
			else
				System.out.println("\tUnrecognized command\""+itemlist[0]+"\"!");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DirectedWieghtGrahp<String> enes = new DirectedWieghtGrahp<String>();
	
		ArrayList<String> distance = reader(args[0]);
		ArrayList<String> topology = reader(args[1]);
		ArrayList<String> command = reader(args[2]);
		
		railway(distance,enes);
		topology(topology,enes);
		commands(command,enes);
		//System.out.println(enes.getVertexEdge("0","4").isActive);
		//enes.getVertexEdge("0","4").isActive=true;
		//System.out.println(enes.getVertexEdge("0","4").isActive);
		//System.out.println(enes.size());
	}	

}
